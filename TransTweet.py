﻿#coding:utf-8

import urllib,urllib2
import re
from BeautifulSoup import BeautifulSoup

class TransTweet():
    def __init__(self,tweet):
        self.tweet = tweet
    
    def get_trans_source(self):
        url = 'http://translate.google.com/translate_t'
        values = {'h1':'zh-CN','ie':'utf-8','text':self.tweet,'langpair':'ja|zh-CN'}
        data = urllib.urlencode(values)
        request = urllib2.Request(url,data)
        request.add_header('User-Agent', "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)")
        response = urllib2.urlopen(request)
        source = response.read()
        response.close()
        return source

    def trans_twi(self):
        source = self.get_trans_source()
        soup = BeautifulSoup(source)
        html_need = soup.findAll('div',{'dir':'ltr'})
        html_content = html_need[1].findAll('span',{'id':'result_box'})
        html_final = html_content[0].findAll('span')
        transtweet = html_final[0].string
        return transtweet
        



if __name__ == '__main__':
    a = TransTweet('test').trans_twi()
    print a
        
