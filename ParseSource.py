﻿#coding:utf-8

import re
from BeautifulSoup import BeautifulSoup,Tag,NavigableString
from GetSource import GetSource
from TransTweet import TransTweet

class ParseSource():
    '''Parse the the webpage source,get the usefull NavigableString'''
    def get_ptags(self):
        '''get the p Tag return a list pTags'''
        doc = GetSource('hatano_yui').source()
        soup = BeautifulSoup(doc)
        pTags = soup.findAll('p',{'class':'js-tweet-text'})
        return pTags
    
    def get_tweetstr(self):
        '''get the NavigableString return tweetStr'''
        pTags = self.get_ptags()
        pTag = []
        tweets = []
        for tag in pTags:
            tag_contents = tag.contents
            for elem in tag_contents:
                if isinstance(elem,Tag):
                    li = [str(i.string)for i in elem]
                    li_str = ''.join(li)
                    pTag.append(li_str)
                else:
                    elem_str = elem.string
                    if elem_str != ' ' and elem_str != u'\n':
                        pattern = re.compile(r'\s{2,}')
                        elem_str = pattern.sub('',elem_str) 
                        a = TransTweet(elem_str.encode('utf-8'))
                        tweet = a.trans_twi()
                        pTag.append(elem_str)
                        tweets.append(str(tweet))
        return pTag,tweets
        


if __name__ == '__main__':
    print '\n'.join(ParseSource().get_tweetstr()[1])
    #print ParseSource().get_tweetstr()[1]
    #print '\n'.join(ParseSource().get_tweetstr()[1])
    #print ParseSource().get_ptags()
