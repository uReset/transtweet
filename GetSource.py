﻿#coding:utf-8

import urllib2

class GetSource():
    def __init__(self,userid='ureset'):
        self.userid = userid
        
    def source(self):
        url = 'https://twitter.com/' + self.userid
        request = urllib2.Request(url)
        request.add_header = {}
        response = urllib2.urlopen(request)
        source = response.read()
        response.close()
        return source
        
        
if __name__ == '__main__':
    source = GetSource('hatano_yui')
    doc = file('source.html','w')
    doc.write(source.source())
    doc.close()
